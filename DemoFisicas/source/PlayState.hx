package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.addons.nape.FlxNapeSpace;
import flixel.addons.nape.FlxNapeSprite;
import nape.callbacks.InteractionListener;
import nape.geom.Vec2;
import nape.phys.BodyType;
import nape.callbacks.CbType;
import nape.callbacks.CbEvent;
import nape.callbacks.InteractionType;
import nape.callbacks.InteractionCallback;
import nape.callbacks.InteractionListener;

class PlayState extends FlxState
{
	var _bg : FlxSprite;
	var CBODY1 : CbType;
	var _collisionListener : InteractionListener;
	var _lbl : FlxText;
	
	override public function create():Void
	{
		super.create();
		
		
		//Nape
		FlxNapeSpace.init();
		FlxNapeSpace.space.gravity = new Vec2(0, 600);
		FlxNapeSpace.createWalls();
		
		//Collisions
		CBODY1 = new CbType();
		_collisionListener = new InteractionListener(CbEvent.BEGIN,InteractionType.COLLISION,CBODY1,CBODY1,onCollide);
		FlxNapeSpace.space.listeners.add(_collisionListener);

		
		//BG
		_bg = new FlxSprite(0, 0, "assets/images/BG.png");
		add(_bg);
		
		//Text
		_lbl = new FlxText(0,0,0,"Las cajas han colisionado",24);
		
		
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		if(FlxG.mouse.justPressed){
			var _box = new FlxNapeSprite(FlxG.mouse.x, FlxG.mouse.y);
			_box.loadGraphic("assets/images/Box.png");
			_box.createRectangularBody(_box.width,_box.height,BodyType.DYNAMIC);
			_box.body.space = FlxNapeSpace.space;
			_box.body.cbTypes.add(CBODY1);
			_box.setBodyMaterial(0.1,0.2,0.4,1,0.001);
			add(_box);
		}
	}
	
	private function onCollide(i:InteractionCallback):Void
	{
		add(_lbl);
	}
}
